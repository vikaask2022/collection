package com.google.failsafefast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class Demo {

	public static void main(String[] args) {
		
CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();
		
		list.add(10);
		list.add(35);
		list.add(67);
		list.add(8);
		
		Iterator<Integer> iterator = list.iterator();
		
	
		while(iterator.hasNext()) {
			Integer next = iterator.next();
			if(next == 67) {
			iterator.remove();
			}
		}
		System.out.println(list);
	}
	

}
