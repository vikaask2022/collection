package com.google.comparator;

import java.util.ArrayList;
import java.util.Collections;

public class Demo{

	public static void main(String[] args) {
		
		ArrayList<Employee> list = new ArrayList<>();
		list.add(new Employee(101, "vishal", 25000, 25));
		list.add(new Employee(102, "suraj", 45000, 23));
		list.add(new Employee(103, "yougesh", 65000, 28));
		list.add(new Employee(104, "vikas", 20000, 21));
		list.add(new Employee(105, "priya", 15000, 35));

		Collections.sort(list, new NameCompare());
		
		for(Employee empList: list) {
			System.out.println(empList);
		}
		
		System.out.println("*****************");
		
		Collections.sort(list, new SalaryCompare());
		
		for(Employee empList: list) {
			System.out.println(empList);
		}
	}



}
