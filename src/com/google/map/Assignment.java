package com.google.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Assignment {

	public static void main(String[] args) {
		
		ArrayList<String> maharashtra = new ArrayList<>();
		maharashtra.add("Pune");
		maharashtra.add("latur");
		maharashtra.add("Nashik");
		
		ArrayList<String> karnataka = new ArrayList<>();
		karnataka.add("belgaon");
		karnataka.add("bangalore");
		karnataka.add("hampi");
		
		ArrayList<String> madhyaPradesh = new ArrayList<>();
		madhyaPradesh.add("indore");
		madhyaPradesh.add("bhopal");
		
		ArrayList<String> uttarPradesh = new ArrayList<>();
		uttarPradesh.add("varansi");
		uttarPradesh.add("bihar");
		uttarPradesh.add("lukhnow");
		uttarPradesh.add("noida");
		
		HashMap<String, ArrayList<String>> state = new HashMap<>();
		
		state.put("MH", maharashtra);
		state.put("KA", karnataka);
		state.put("MP", madhyaPradesh);
		state.put("UP", uttarPradesh);
		
		HashMap<String, HashMap<String, ArrayList<String>> > country = new HashMap<>();
		
		country.put("India", state);
		
		
		
		Set<String> keySet = country.keySet();
		
		for(Object obj : keySet) {
			System.out.println("key :"+obj);
			System.out.println("value :"+country.get(obj));
		}
		
	}
	

}
