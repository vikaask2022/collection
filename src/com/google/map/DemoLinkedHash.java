package com.google.map;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Spliterator;

public class DemoLinkedHash {

	public static void main(String[] args) {

       LinkedHashMap<Integer, String> map = new LinkedHashMap<>();
       
       map.put(1, "ajay");
       map.put(4, "anita");
       map.put(2, "aman");
       map.put(10, "jay");
       
       Set<Integer> keySet = map.keySet();
       
//       for(Object mapSet : keySet) {
//    	   System.out.println("key : "+mapSet);
//    	   System.out.println("value : " +map.get(mapSet));
//    	   
//       }
       
       Collections.synchronizedMap(map);
       
       Iterator<Integer> iterator = keySet.iterator();
       
       while(iterator.hasNext()) {
    	   Integer next = iterator.next();
    	   System.out.println("key : "+next);
    	   System.out.println("value : "+map.get(next));
       }
	}

}
