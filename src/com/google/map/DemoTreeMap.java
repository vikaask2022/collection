package com.google.map;

import java.util.Collections;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;

public class DemoTreeMap {

	public static void main(String[] args) {
		
		TreeMap tree = new TreeMap();
		
		tree.put(12, "rani");
		tree.put(95, "ranita");
		tree.put(52, "rai");
		tree.put(4, "vish");
//		tree.put("radha", "rame");
		tree.put(27, "ani");
		tree.put(12, "rani");
//		tree.put(null, "rohit");
		
		
		Collections.synchronizedMap(tree);
		
		NavigableSet<Integer> keySet = tree.descendingKeySet();
//		Set<Integer> keySet = tree.keySet();
		
		
		
		for(Object set : keySet) {
			System.out.println("key : "+set);
			System.out.println("value : "+tree.get(set));
		}

	}

}
