package com.google.map;

import java.util.HashMap;
import java.util.Set;

public class DemoHash {

	public static void main(String[] args) {

		HashMap map = new HashMap<Integer, String>();
		
		map.put(1, 10);
		map.put(null, 20);
		map.put(2, "vishal");
		map.put(null, 40);
//		map.put(2, 50);
		map.put(3, "raj");
		map.put(4, 'c');
		
		
		Set keySet = map.keySet();
		
//		Iterator iterator = keySet.iterator();
		
//		while(iterator.hasNext()) {
//			
//			Object next = iterator.next();
//			
//			
//			System.out.println("key : "+next); // key given 1 
//			System.out.println("Value : "+map.get(next));
//			
//		}
		
		
		for(Object obj : keySet) {
			System.out.println("key : "+ obj);
			System.out.println("value : "+map.get(obj));
		}
	}

}
