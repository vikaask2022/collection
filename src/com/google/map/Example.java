package com.google.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Example {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<Integer, String> map = new HashMap<>();
		
		map.put(10, "rahul");
		map.put(15, "raj");
		map.put(20, "ram");
		map.put(21, "rani");
		map.put(30, "son");
		
		Iterator<Entry<Integer, String>> iterator = map.entrySet().iterator();
		
		while(iterator.hasNext()) {
			Entry<Integer, String> next = iterator.next();
			System.out.println("key : " +next.getKey());
			System.out.println("value : " +next.getValue());
		}
		
	}

}
