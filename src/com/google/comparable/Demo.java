package com.google.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

public class Demo {

	public static void main(String[] args) {
		
		ArrayList<Employee> list = new ArrayList<>();
		
		list.add(new Employee(101, "vishal", 25000));
		list.add(new Employee(102, "suraj", 80000));
		list.add(new Employee(103, "yogesh", 65000));
		list.add(new Employee(104, "vikas", 20000));
		
		
		
		System.out.println("before sorting");
		
		for(Employee empList : list) {
			System.out.println(empList);
		}
		System.out.println("**************");
		

		Collections.sort(list);
		
			
		
		for(Employee empList : list) {
			System.out.println(empList);
		}
		System.out.println("Vishakl");
		

	}

}
