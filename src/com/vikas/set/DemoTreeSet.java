package com.vikas.set;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class DemoTreeSet {

	public static void main(String[] args) {
		
		TreeSet<String> set = new TreeSet<String>();
		
		set.add("Pune");
		set.add("Mumbai");
		set.add("Bombay");
		set.add("Auran");
		set.add("Nashik");
		set.add("Latur");
		set.add("Shirdi");
		set.add("Usmanpura");
		set.add("Pune");
//		set.add('c');
		
		
		Iterator<String> descendingIterator = set.descendingIterator();
//		NavigableSet<String> descendingSet = set.descendingSet();
//		
//		SortedSet<String> sortedSet = new TreeSet<String>(set);
//		System.out.println(descendingSet);
		
//		Iterator<String> setIterator = set.iterator();
		
//		**** iterator wont have methods to support for Each ******
		
		
//		while(setIterator.hasNext()) {
//			System.out.println(setIterator.next());
//		}
		
		while(descendingIterator.hasNext()) {
			System.out.println(descendingIterator.next());
			
		}
		
//		for(String setData : set) {
//			System.out.println(setData);
//		}

	}

}
