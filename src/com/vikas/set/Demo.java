package com.vikas.set;

import java.util.Collections;
import java.util.HashSet;

public class Demo {

	public static void main(String[] args) {

		HashSet<Integer> list = new HashSet<Integer>();
		
		list.add(15);
		list.add(30);
		list.add(32);
		list.add(32);
		list.add(35);
		list.add(95);
		list.add(57);
		list.add(78);
		list.add(78);
		list.add(61);
		list.add(1);
		list.add(10);
		list.add(11);
		list.add(23);
		list.add(22);
		list.add(70);
		list.add(90);
		list.add(5);
		list.add(3);
		list.add(3);
		list.add(13);
		list.add(null);
		list.add(null);
		
		
		
		System.out.println(list);
		System.out.println(list.size());
		
		
	}
	
	

	@Override
	public String toString() {
		return "Demo [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}


	
}
