package com.vikas.set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class Demo2 {

	public static void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<>();
		
		
		list.add(15);
		list.add(30);
		list.add(32);
		list.add(32);
		list.add(35);
		list.add(95);
		list.add(57);
		list.add(78);
		list.add(78);
		list.add(61);
		list.add(1);
		list.add(10);
		list.add(11);
		list.add(23);
		list.add(22);
		list.add(70);
		list.add(90);
		list.add(5);
		list.add(3);
		list.add(3);
		list.add(13);
		list.add(null);
		list.add(null);
		
		
		
		LinkedHashSet<Integer> list1 = new LinkedHashSet<Integer>(list);
		
		System.out.println("hash set list"+list1);
		
		HashSet<Integer> list3 = new HashSet<>(list);
		
//		System.out.println("array list" +list);
//		System.out.println(list1);
		
//		System.out.println("array list size"+list.size());
//		System.out.println(list1.size());
//		System.out.println("hash set sizze"+list3.size());
		
		System.out.println("using for each");
		for(Integer hash : list3) {
			System.out.println(hash);
		}
		

	}

}
