package com.vikas.arraylist;

import java.util.ArrayList;

public class DemoArrayList {	
	
	
	public static void main(String [] args) {
		
		ArrayList list = new ArrayList();
		
		list.add(10);
		list.add(10);
		list.add("vikas");
		list.add('c');
		list.add(null);
		list.add(null);
		
		list.remove(5);
		
		System.out.println(list);
		
	}

}
