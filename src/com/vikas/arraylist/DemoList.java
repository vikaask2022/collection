package com.vikas.arraylist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

public class DemoList {

	public static void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<>();
		
		list.add(10);
		list.add(35);
		list.add(67);
		list.add(8);
		list.add(92);
		list.add(51);
		list.add(22);
		list.add(45);
		list.add(99);
		
	
//		
//		TreeSet<Integer> set = new TreeSet<>(list);
//		
//		Iterator<Integer> descendingIterator = set.descendingIterator();
//		
//		
//		while(descendingIterator.hasNext()) {
//			System.out.println(descendingIterator.next());
//		}
		
		System.out.println(list);
//		
//		list.contains(45);
//		int indexOf = list.indexOf(45);
//		System.out.println(indexOf);
//		System.out.println(list.size());
		
//		list.add((list.size()/2), 56);
//		System.out.println(list);
		
	}

}
