package com.vikas.arraylist;

import java.util.ArrayList;
import java.util.Iterator;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Employee> list = new ArrayList<Employee>();
		
		list.add(new Employee(101, "vijay", 25));
		list.add(new Employee(102, "vinit", 55));
		list.add(new Employee(103, "vishal", 75));
		
//		Iterator<Employee> iterator = list.iterator();
		
//		while(iterator.hasNext()) {
//			Employee next = iterator.next();
//			System.out.println(next);
//		}
		
		for(Employee e : list) {
			System.out.println(e);
		}
		
	}

}
