package com.vikas.arraylist;

import java.util.ArrayList;
import java.util.Iterator;

public class Example {
	
	//program to copy elements from one arraylist to another

	public static void main(String[] args) {


		ArrayList<Integer> list1 = new ArrayList<Integer>();
		
		list1.add(10);
		list1.add(20);
		list1.add(30);
		
		ArrayList<Integer> list2 = new ArrayList<Integer>();
		
		list2.add(40);
		list2.add(50);
		list2.add(65);
	
		
		
		
//		list1.addAll(list2);
		
		
		
//		using sysout(not allowed)
//		System.out.println(list1);
		
		
//		1. for each
		
//		for(int d : list1) {
//			System.out.println(d);
//		}

//		2. iterator
		
		Iterator<Integer> iterator = list1.iterator();
		
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
